function myClick(a)
{
    url = a ? '/radioOn' : '/radioOff'
          $.getJSON(url,
              function(data) {
            //do nothing
          });
          return false;
}

function myCommand()
{
    $.post('/myCommand','1337')       
}

function changeRadio(a)
{
    var b = 'station' + a
    
    document.getElementById("combobox").innerHTML = document.getElementById(b).innerText + '<span class="caret"></span>'
    $.post('/changeRadio',b)
}


function runCommand(d, b) {
    var a = $("#commandForm");
    d += "\n";
    url = b ? "/command_silent" : "/command";
    var c = $.post(url, d);

    if (d == "progress\n"){
        c.done(function(e) {
            $("#progressShow").empty();
            $.each(e.split("\n"), function(f) {
                $("#progressShow").append(this + "<br/>")
            })
        })
    }
    else if (!b) {
        c.done(function(e) {
            $("#result").empty();
            $.each(e.split("\n"), function(f) {
                $("#result").append(this + "<br/>")
            })
        })
    }
}

function showTemp(d, b){
    var a = $("#commandForm");
    d += "\n";
    url = b ? "/command_silent" : "/command";
    var c = $.post(url, d);
    if (!b) {
        c.done(function(e) {
            $("#tempShow").empty();
            var res = e.split(/@\d+/);
            res[0] = res[0].substr(3);
            for(i=0;i<res.length;i++){
                res[i] = res[i].replace("T","L");
                res[i] = res[i].replace("Q","R");
            }
            for(i=0;i<res.length;i++){
                document.getElementById("tempShow").innerHTML += res[i] + "<br/>";
            }
        })
    }
}

function runCommandSilent(a) {
    runCommand(a, true)
}

function runCommandCallback(c, d) {
    var b = "/command";
    c += "\n";
    var a = $.post(b, c, d)
}

function jogXYClick(a) {
    runCommand("G91 G0 " + a + " F" + document.getElementById("xy_velocity").value + " G90", true)
}

function jogZClick(a) {
    runCommand("G91 G0 " + a + " F" + document.getElementById("z_velocity").value + " G90", true)
}

function extrude(g, d, c) {
	var a = (g.currentTarget.id == "extrude_left" || g.currentTarget.id == "reverse_left") ? "E" : "A";
    var f = document.getElementById("extrude_length").value;
    var e = document.getElementById("extrude_velocity").value;
    var h = (g.currentTarget.id == "extrude_left" || g.currentTarget.id == "extrude_right") ? 1 : -1;
    runCommand("T3 G91 G0 " + a + (f * h) + " F" + e + " G90", true)
}

function motorsOff(a) {
    runCommand("M18", true)
}

function heatSet(c) {
    var b = (c.currentTarget.id == "bed_set") ? 140 : 104;
    var d = (c.currentTarget.id == "heat_right_set") ? 1 : 0;
    var a = 0;

    if(b == 104 && c.currentTarget.id == "heat_left_set"){
    	a= document.getElementById("heat_left_value").value;
    }
    else if(b == 104 && c.currentTarget.id == "heat_right_set"){
    	a = document.getElementById("heat_right_value").value;
    }
    else{
    	a = document.getElementById("bed_value").value;
    }
    runCommand("T" + d + " M" + b + " S" + a + " T3", true)
}

function heatOff(b) {
    var a = (b.currentTarget.id == "bed_off") ? 140 : 104;
    var c = 3;
    if(b.currentTarget.id == "heat_left_off"){
    	c = 0;
    }
    else if(b.currentTarget.id == "heat_right_off"){
    	c = 1;
    }
    runCommand("T" + c + " M" + a + " S0 T3", true)
}

function getTemperature() {
    showTemp("M105", false)
}

function handleFileSelect(a) {
    var d = a.target.files;
    var b = [];
    for (var c = 0, e; e = d[c]; c++) {
        b.push("<li><strong>", escape(e.name), "</strong> (", e.type || "n/a", ") - ", e.size, " bytes, last modified: ", e.lastModifiedDate ? e.lastModifiedDate.toLocaleDateString() : "n/a", "</li>")
    }
    document.getElementById("list").innerHTML = "<ul>" + b.join("") + "</ul>"
}

function upload() {
    $("#progress").empty();
    $("#uploadresult").empty();
    var b = document.getElementById("files").files[0];
    var a = new FileReader();
    a.readAsBinaryString(b);
    a.onloadend = function(c) {
        xhr = new XMLHttpRequest();
        xhr.open("POST", "upload", true);
        xhr.setRequestHeader("X-Filename", b.name);
        XMLHttpRequest.prototype.mySendAsBinary = function(k) {
            var h = new ArrayBuffer(k.length);
            var f = new Uint8Array(h, 0);
            for (var g = 0; g < k.length; g++) {
                f[g] = (k.charCodeAt(g) & 255)
            }
            if (typeof window.Blob == "function") {
                var e = new Blob([h])
            } else {
                var j = new(window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder)();
                j.append(h);
                var e = j.getBlob()
            }
            this.send(e)
        };
        var d = xhr.upload || xhr;
        d.addEventListener("progress", function(i) {
            var f = i.position || i.loaded;
            var h = i.totalSize || i.total;
            var g = Math.round((f / h) * 100);
            $("#progress").empty().append("uploaded " + g + "%")
        });
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    $("#uploadresult").empty().append("Uploaded Ok")
                } else {
                    $("#uploadresult").empty().append("Uploaded Failed")
                }
            }
        };
        xhr.mySendAsBinary(c.target.result)
    }
}

function playFile(a) {
    runCommandSilent("play /sd/" + a)
}

function refreshFiles() {
    document.getElementById("fileList").innerHTML = "";
    runCommandCallback("M20", function(a) {
        $.each(a.split("\n"), function(c) {
            var e = this.trim();
            if (e.match(/\.g(code)?$/)) {
                var d = document.getElementById("fileList");
                var g = d.insertRow(-1);
                var b = g.insertCell(0);
                var f = document.createTextNode(e);
                b.appendChild(f);
                b = g.insertCell(1);
                b.innerHTML = "[<a href='javascript:void(0);' onclick='playFile(\"" + e + "\");'>Play</a>]"
            }
        })
    })
};