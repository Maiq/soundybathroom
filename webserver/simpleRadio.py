import subprocess as sp
class Radio():
    def __init__ (self):
        self.onlineStreams = ['http://antgre-01.cdn.eurozet.pl:8004/','http://antgre-01.cdn.eurozet.pl:8008/']
        self.radioIndex = 0
        self.procNo = None

    def play(self):
        print("Light on!")
        if self.procNo != None:
            self.stop(1)
        self.procNo = sp.Popen(['mpg123',self.onlineStreams[self.radioIndex]])
            

    def stop(self,internal=0):
        if internal == 0:
            print("Blackout")
        
        try:
            self.procNo.kill()
            self.procNo = None
        except:
            pass

    def changeStation(self,index):
        self.radioIndex = index
    
    def isPlaying(self):
        if self.procNo != None:
            return True
        else:
            return False 
            

