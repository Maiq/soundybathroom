from flask import Flask, render_template,request, Response
import simpleRadio as radio

from gpiozero import Button
from signal import pause


app = Flask(__name__)
myRadio = radio.Radio()

lightSwitch = Button(4,pull_up=False,bounce_time=1)
lightSwitch.when_pressed = myRadio.play
lightSwitch.when_released = myRadio.stop

@app.route('/')
def hello_world():
    toiletFree = myRadio.isPlaying()
    return render_template('index.html')


@app.route('/radioOn')
def myClickFun():
    myRadio.play()
    return render_template('index.html')

@app.route('/radioOff')
def myClickFun1():
    myRadio.stop()
    return render_template('index.html')


@app.route('/changeRadio',methods = ['POST', 'GET'])
def commandHello():
    if request.method == 'POST':
        value = request.get_data()
        newIndex = int(value[-1])
        myRadio.changeStation(newIndex)
        myRadio.play()
        return render_template('index.html')


                